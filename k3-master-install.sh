#!/bin/bash
export K3S_KUBECONFIG_MODE="644"
export INSTALL_K3S_EXEC=" --no-deploy servicelb --no-deploy traefik"
curl -sfL https://get.k3s.io | sh -
echo "Node Token, copy this, you'll need it for the agents"
sudo cat /var/lib/rancher/k3s/server/node-token
