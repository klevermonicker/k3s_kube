#!/bin/bash
echo "Creating k8s directory"
mkdir ~/.kube
echo "Grabbing k8s config"
scp pi@k3s-master:/etc/rancher/k3s/k3s.yaml ~/.kube/rpi_home.config
echo "Adjusting config so it works outside your cluster"
sed -i 's/127\.0\.0\.1/X\.X\.X\.X/g' ~/.kube/rpi_home.config
