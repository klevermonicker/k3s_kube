#!/bin/bash
echo "Installing metaldb"
echo "Mode: Layer 2"
echo "IP Range: 192.168.138.10-20"
helm install metallb stable/metallb --namespace kube-system \
  --set configInline.address-pools[0].name=default \
  --set configInline.address-pools[0].protocol=layer2 \
  --set configInline.address-pools[0].addresses[0]=192.168.138.10-192.168.138.20


echo "Installing traefikv2"
helm install traefik traefik/traefik --namespace kube-system
